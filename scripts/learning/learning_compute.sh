#!/bin/bash

# Edit architecture values
export CORES=112
export SOCKETS=2
export CORES_PER_SOCKET=56

# Edit learning phase parameters
export EAR_MIN_P_STATE=1
export EAR_MAX_P_STATE=6
export EAR_TIMES=3

# Edit output options
export OUT_OUT="$HOME/out"
export OUT_ERR="$HOME/err"

# Non-edit region
export HOSTLIST="$(echo $(cat $1))"
export BENCHS_MODE="learning"

# TODO: adapt
module purge
module load 2022
module load intel-compilers/2022.1.0
module load iimpi/2022a
module load imkl/2022.1.0
module load ear

ulimit -s unlimited

# TODO: adapt
for i in ${HOSTLIST}
do
	echo "Executing learning phase in node=${i}"
	sbatch -w ${i} -N 1 --ear=on --exclusive --export=ALL --time=10:00:00 -o $OUT_OUT.${i}.%j -e $OUT_ERR.${i}.%j \
		$PWD/helpers/kernels_iterator.sh
done
