#!/bin/bash

if [[ $# -ne 1 ]]
then
        echo -e "Usage: hostlist"
        echo -e "\thostlist: a host name list file"
        exit 1
fi

if [ ! -f $1 ]
then
    echo "Error, this nodelist file doesn't exists"
fi

if [ -z $EAR_INSTALL_PATH ]
then
    echo -e "ERROR: EAR_INSTALL_PATH environment variable is not set."
    echo -e "TIP! Load the EAR environment module."
    exit 1
fi

# Edit output options
export EAR_COEFFS_PATH="$HOME/ear_coeffs/"

# Edit minimum frequency (in KHz)
export MIN_FREQUENCY=1000000

# Non-edit region
export HOSTLIST="$(echo $(cat $1))"
mkdir -p $EAR_COEFFS_PATH

for i in ${HOSTLIST}
do
    echo "Computing coefficients for node=${i}"
    srun -w ${i} -n 1 $EAR_INSTALL_PATH/bin/tools/coeffs_compute $EAR_COEFFS_PATH $MIN_FREQUENCY ${i}
done
