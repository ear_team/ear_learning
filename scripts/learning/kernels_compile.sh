#!/bin/bash

# Edit architecture values
export CORES=112
export SOCKETS=2
export CORES_PER_SOCKET=56

# Non-edit region
export EAR_MIN_P_STATE=1
export EAR_MAX_P_STATE=1
export EAR_TIMES=1

# Non-edit region
export BENCHS_MODE="compile"

# TODO: adapt
module purge
module load 2022
module load intel-compilers/2022.1.0
module load iimpi/2022a
module load imkl/2022.1.0

mkdir -p ../../bin

$PWD/helpers/kernels_iterator.sh
