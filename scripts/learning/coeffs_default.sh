#!/bin/bash

if [[ $# -ne 2 ]]
then
        echo -e "Usage: hostlist island"
        echo -e "\thostlist: a host name list file"
        echo -e "\tisland: the number of the island"
        exit 1
fi

if [ ! -f $1 ]
then
	echo -e "Error, the nodelist file doesn't exists"
	exit -1
fi

if [ -z $EAR_INSTALL_PATH ]
then
    echo -e "ERROR: EAR_INSTALL_PATH environment variable is not set."
    echo -e "TIP! Load the EAR environment module."
    exit 1
fi

# Edit output options
export EAR_COEFFS_PATH="$HOME/ear_coeffs"

# Non-edit region
export HOSTLIST="$(echo $(cat $1))"
export HOSTLIST_PLAIN=""

# Converting a line break list into a space splitted list
for i in ${HOSTLIST}
do
	export HOSTLIST_PLAIN="${i} $HOSTLIST_PLAIN"
done

$EAR_INSTALL_PATH/bin/tools/coeffs_default $EAR_COEFFS_PATH/island$2/ $HOSTLIST_PLAIN
