#!/bin/bash

# Non edit region
export BENCHS_PWD="$PWD"
source $BENCHS_PWD/helpers/kernels_executor.sh

# Running the learning phase
echo "Executing from ${EAR_MIN_P_STATE} to ${EAR_MAX_P_STATE} ${EAR_TIMES} times"
for (( i=${EAR_MIN_P_STATE}; i<=${EAR_MAX_P_STATE}; i++ ))
do
    for (( j=0; j<${EAR_TIMES}; j++ ))
    do
        echo "Pstate ${i} try ${j}"
        export EAR_P_STATE=${i}
        learning_phase lu-mpi C
        learning_phase lu-mpi D
        learning_phase ep D
        learning_phase mg D
        learning_phase ua C
        learning_phase ua D
        learning_phase bt-mz C
        learning_phase bt-mz D
        learning_phase sp-mz C
        learning_phase sp-mz D
        learning_phase dgemm
       	learning_phase stream
    done
done
