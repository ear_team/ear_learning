#!/bin/bash

if [[ $# -eq 0 ]]
then
        echo -e "Usage: hostlist"
        echo -e "    hostlist: a hostname list of the nodes"
        exit 1
fi

if [ ! -f $1 ]
then
    echo "Error, thie nodelist file doesn't exists"
fi

if [ -z $EAR_INSTALL_PATH ]
then
    echo -e "ERROR: EAR_INSTALL_PATH environment variable is not set."
    echo -e "TIP! Load the EAR environment module."
    exit 1
fi

# Edit architecture values
export CORES=112
export SOCKETS=2
export CORES_PER_SOCKET=56

# Edit learning phase parameters
export EAR_MIN_P_STATE=1
export EAR_MAX_P_STATE=1
export EAR_TIMES=1

# TODO: adapt
module purge
module load 2022
module load intel-compilers/2022.1.0
module load iimpi/2022a
module load imkl/2022.1.0
module load ear

ulimit -s unlimited

# Edit output options
export OUT_OUT="$HOME/test-out"
export OUT_ERR="$HOME/test-err"

# Non-edit region
export HOSTLIST="$(echo $(cat $1))"
export BENCHS_MODE="test"

# TODO: adapt
for i in ${HOSTLIST}
do
	echo "Executing learning tests in node=${i}"
	sbatch -w ${i} -N 1 --ear=on  --exclusive --export=ALL --time=1:00:00 -o $OUT_OUT.${i}.%j -e $OUT_ERR.${i}.%j \
		$PWD/helpers/kernels_iterator.sh
done
