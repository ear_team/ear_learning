Energy Aware Runtime scripts
----------------------------
A set of bash scripts are provided to make some processes as simple and contained as possible. Two sets of scripts are provided:
1) Learning phase scripts to automatize the learning phase process. More information in [learning phase page](./learning/README.md).
2) Tools scripts to expand the tool binaries utility. More information in [tools scripts page](./tools/README.md).