# EAR learning phase

This is a phase prior to the normal EAR utilization, and it is called learning phase since is a kind of hardware characterization of the nodes.
Please visit [the wiki page](https://gitlab.bsc.es/ear_team/ear_learning/-/wikis/home) for a detailed guide.

License
-------
EAR is a open source software and it is licensed under both the BSD-3 license for individual/non-commercial
use and EPL-1.0 license for commercial use. Full text of both licenses can be
found in COPYING.BSD and COPYING.EPL files.

Contact: [ear-support@bsc.es](mailto:ear-support@bsc.es)

